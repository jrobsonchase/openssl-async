use std::mem;
use std::pin::Pin;

use openssl::ssl::{self, ConnectConfiguration, SslConnector};

use futures::io::{AsyncRead, AsyncWrite};
use futures::prelude::*;
use futures::task::{Context, Poll};

use async_stdio::*;

use crate::{HandshakeError, MidHandshakeSslStream, SslStream};

/// Extension trait for [SslConnector] to allow connections to be initiated
/// asynchronously.
pub trait SslConnectorExt {
    /// Asynchronously initiate the SSL connection
    fn connect_async<S: AsyncRead + AsyncWrite>(&self, domain: &str, stream: S) -> ConnectAsync<S>;
}

/// Extension trait for [ConnectConfiguration] to allow connections to be
/// initiated asynchronously.
pub trait ConnectConfigurationExt {
    /// Asynchronously initiate the SSL connection
    fn connect_async<S: AsyncRead + AsyncWrite>(self, domain: &str, stream: S) -> ConnectAsync<S>;
}

impl ConnectConfigurationExt for ConnectConfiguration {
    fn connect_async<S: AsyncRead + AsyncWrite>(self, domain: &str, stream: S) -> ConnectAsync<S> {
        ConnectAsync(ConnectInner::Init(self, domain.into(), stream))
    }
}

impl SslConnectorExt for SslConnector {
    fn connect_async<S: AsyncRead + AsyncWrite>(&self, domain: &str, stream: S) -> ConnectAsync<S> {
        match self.configure() {
            Ok(s) => s.connect_async(domain, stream),
            Err(e) => ConnectAsync(ConnectInner::Error(HandshakeError::SetupFailure(e))),
        }
    }
}

/// The future returned from [SslConnectorExt::connect_async]
///
/// Resolves to a [SslStream]
pub struct ConnectAsync<S>(ConnectInner<S>);

enum ConnectInner<S> {
    Init(ConnectConfiguration, String, S),
    Handshake(MidHandshakeSslStream<S>),
    Error(HandshakeError<S>),
    Done,
}

impl<S: AsyncRead + AsyncWrite + Unpin> Future for ConnectAsync<S> {
    type Output = Result<SslStream<S>, HandshakeError<S>>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = Pin::get_mut(self);

        match mem::replace(&mut this.0, ConnectInner::Done) {
            ConnectInner::Init(config, domain, stream) => {
                let (stream, ctrl) = AsStdIo::new(stream, cx.waker().into());
                match config.connect(&domain, stream) {
                    Ok(inner) => Poll::Ready(Ok(SslStream { inner, ctrl })),
                    Err(ssl::HandshakeError::WouldBlock(inner)) => {
                        this.0 = ConnectInner::Handshake(MidHandshakeSslStream::new(inner, ctrl));
                        Poll::Pending
                    }
                    Err(e) => Poll::Ready(Err(HandshakeError::from_ssl(e, ctrl).unwrap())),
                }
            }
            ConnectInner::Handshake(mut handshake) => match Pin::new(&mut handshake).poll(cx) {
                Poll::Ready(result) => Poll::Ready(result),
                Poll::Pending => {
                    this.0 = ConnectInner::Handshake(handshake);
                    Poll::Pending
                }
            },
            ConnectInner::Error(e) => Poll::Ready(Err(e)),
            ConnectInner::Done => panic!("accept polled after completion"),
        }
    }
}
