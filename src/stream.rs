use std::fmt;
use std::io::{self, Read, Write};
use std::ops::{Deref, DerefMut};
use std::pin::Pin;

use openssl::ssl;

use futures::io::{AsyncRead, AsyncWrite};
use futures::ready;
use futures::task::{Context, Poll};

use async_stdio::*;

/// An asynchronous SSL stream
pub struct SslStream<S> {
    pub(crate) inner: ssl::SslStream<AsStdIo<S>>,
    pub(crate) ctrl: WakerCtrl,
}

impl<S> Deref for SslStream<S> {
    type Target = ssl::SslStream<AsStdIo<S>>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<S> DerefMut for SslStream<S> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<S: fmt::Debug> fmt::Debug for SslStream<S> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("SslStream")
            .field("inner", &self.inner)
            .finish()
    }
}

impl<S> AsyncRead for SslStream<S>
where
    S: Unpin + AsyncRead + AsyncWrite,
{
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        let this = self.get_mut();

        this.ctrl.register(cx.waker());

        this.inner.read(buf).into_poll()
    }
}

impl<S> AsyncWrite for SslStream<S>
where
    S: AsyncWrite + AsyncRead + Unpin,
{
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<io::Result<usize>> {
        let this = self.get_mut();

        this.ctrl.register(cx.waker());

        this.inner.write(buf).into_poll()
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        let this = self.get_mut();

        this.ctrl.register(cx.waker());

        this.inner.flush().into_poll()
    }

    fn poll_close(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        ready!(self.as_mut().poll_flush(cx)?);

        self.get_mut()
            .inner
            .get_mut()
            .with_context(|stream, cx| stream.poll_close(cx))
    }
}

#[cfg(feature = "tokio")]
mod tokio {
    use super::*;

    impl<S> tokio_io::AsyncRead for SslStream<S>
    where
        S: Unpin + AsyncRead + AsyncWrite,
    {
        fn poll_read(
            self: Pin<&mut Self>,
            cx: &mut Context<'_>,
            buf: &mut [u8],
        ) -> Poll<io::Result<usize>> {
            AsyncRead::poll_read(self, cx, buf)
        }
    }

    impl<S> tokio_io::AsyncWrite for SslStream<S>
    where
        S: AsyncWrite + AsyncRead + Unpin,
    {
        fn poll_write(
            self: Pin<&mut Self>,
            cx: &mut Context<'_>,
            buf: &[u8],
        ) -> Poll<io::Result<usize>> {
            AsyncWrite::poll_write(self, cx, buf)
        }

        fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
            AsyncWrite::poll_flush(self, cx)
        }

        fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
            AsyncWrite::poll_close(self.as_mut(), cx)
        }
    }
}
