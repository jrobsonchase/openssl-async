# Async Wrappers for OpenSSL

[![version](https://img.shields.io/crates/v/openssl-async.svg)](https://crates.io/crates/openssl-async/)
[![documentation](https://docs.rs/openssl-async/badge.svg)](https://docs.rs/openssl-async/)
[![license](https://img.shields.io/crates/l/openssl-async.svg)](https://crates.io/crates/openssl-async/)

## License

Licensed under

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be licensed as above, without any additional terms or
conditions.
