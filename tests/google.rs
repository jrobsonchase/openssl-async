use std::net::ToSocketAddrs;

use futures::prelude::*;
use openssl::ssl::{SslConnector, SslMethod};
use openssl_async::SslConnectorExt;
use runtime::net::TcpStream;

#[runtime::test]
async fn fetch_google() {
    let addr = "google.com:443".to_socket_addrs().unwrap().next().unwrap();

    let connector = SslConnector::builder(SslMethod::tls()).unwrap().build();

    let socket = TcpStream::connect(&addr).await.unwrap();
    let mut socket = connector
        .connect_async("google.com", socket)
        .await
        .expect("failed to connect");

    socket.write_all(b"GET / HTTP/1.0\r\n\r\n").await.unwrap();
    socket.flush().await.unwrap();

    let mut data = Vec::new();

    socket.read_to_end(&mut data).await.unwrap();

    // any response code is fine
    assert!(data.starts_with(b"HTTP/1.0 "));

    let data = String::from_utf8_lossy(&data);
    let data = data.trim_end();
    assert!(data.ends_with("</html>") || data.ends_with("</HTML>"));
}
